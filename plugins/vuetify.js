import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { aliases, mdi } from 'vuetify/iconsets/mdi'
import '@mdi/font/css/materialdesignicons.css' //icons

export default defineNuxtPlugin(nuxtApp => {
  // Custom theme properties
  const customDarkTheme = {
    dark: false,
    colors: {
      surface: "#15202b",
      primary: "#0179BF",
      secondary: "#FF8181",
      error: "#ff3162",
      info: "#2196F3",
      success: "#4caf50",
      warning: "#fb8c00"
    }
  }

  const vuetify = createVuetify({
    ssr: true,
    components,
    directives,
    icons: {
      defaultSet: 'mdi',
      aliases,
      sets: {
        mdi
      }
    },
    theme: {
      defaultTheme: "customDarkTheme",
      themes: {
        customDarkTheme,
      }
    }
  })

  nuxtApp.vueApp.use(vuetify)
})