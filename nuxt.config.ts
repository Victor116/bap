import { defineNuxtConfig } from 'nuxt'

export default defineNuxtConfig({
  // import styles
  // css: ["@/assets/main"],
  css: [
    'vuetify/lib/styles/main.sass',
    '~/assets/css/index.css'
  ],
  build: {
    transpile: ['vuetify'],
  },
  vite: {
    define: {
      'process.env.DEBUG': false,
    },
  },
  modules: ['@pinia/nuxt'],

  app: {
    head: {
      title: "Ejercicios BAP",
      link: [
        { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      ],
      meta: []
    }
  },

  runtimeConfig:{
    public: {
      urlApi: process.env.NUXT_PUBLIC_API_BASE
    }
  }
})