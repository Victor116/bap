import { useAuthStore } from '~/store/auth'

export default defineNuxtRouteMiddleware((to: any, from: any) =>{
  const isLoggedIn = useCookie<{ token: string }>('token')

  if(!isLoggedIn.value){
    return navigateTo({ path: '/login' })
  } else {
    const authStore = useAuthStore()
    const { setApiToken } = authStore
    setApiToken(isLoggedIn.value.token)
  }

})