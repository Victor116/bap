import { defineStore } from 'pinia';

export const useTasksStore = defineStore({
  id: 'tasks',
  state: () => ({
    filtersList: ['youtube', 'twitch'],
    item: null,
    items: [],
    itemsStatuses: {},
    total: 0,
    page: 1,
    itemsPerPage: 5,
    isLoading: false,
    isLoadingSingle: false,
    isSaving: false,
    isRemoving: false,
    error: ''
  }),
  actions: {
    addValueToFilterList(value: string) {
      this.filtersList.push(value)
    },

    async testConeccion(apiToken: string) {
      const { urlApi } = useRuntimeConfig()
      const options = {
        initialCache: false,
        method: "GET",
        headers: {
          Authorization: `Bearer ${decodeURI(apiToken)}`
        }
      }

      return await useFetch(`${urlApi}/tasks`, options)
    },

    async getTasks(apiToken: string) {
      const { urlApi } = useRuntimeConfig()
      const options = {
        initialCache: false,
        method: "GET",
        headers: {
          Authorization: `Bearer ${decodeURI(apiToken)}`
        }
      }

      return await useFetch(`${urlApi}/tasks?token=${decodeURI(apiToken)}`, options)
    },

    async getTasksId(id: any, apiToken: string) {
      const { urlApi } = useRuntimeConfig()
      const options = {
        initialCache: false,
        method: "GET",
        headers: {
          Authorization: `Bearer ${decodeURI(apiToken)}`
        }
      }

      return await useFetch(`${urlApi}/tasks/${id}?token=${decodeURI(apiToken)}`, options)
    },

    async createTask( params: any, apiToken: string ){
      const { urlApi } = useRuntimeConfig()

      const options = {
        initialCache: false,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${decodeURI(apiToken)}`,
        },
        body: params
      }

      return await useFetch(`${urlApi}/tasks`, options)
    },

    async updateTask( params: any, id: any, apiToken: string ){
      const { urlApi } = useRuntimeConfig()

      const options = {
        initialCache: false,
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${decodeURI(apiToken)}`,
        },
        body: params
      }

      return await useFetch(`${urlApi}/tasks/${id}?token=${decodeURI(apiToken)}`, options)
    },

    async deleteTask( id: any, apiToken: string ){
      const { urlApi } = useRuntimeConfig()

      const options = {
        initialCache: false,
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${decodeURI(apiToken)}`,
        },
      }

      return await useFetch(`${urlApi}/tasks/${id}?token=${decodeURI(apiToken)}`, options)
    }
  }
});