import { defineStore } from 'pinia';

export const useToask = defineStore({
  id: 'toask',
  state: () => ({
    message: {
      status: Boolean(false),
      color: String(''),
      message: String('')
    }
  }),
  actions: {
    setToaskValues(message:string, color: string) {
      this.message.message = message
      this.message.status = true
      this.message.color = color
      setTimeout(this.clearValues, 5000);
    },
    clearValues() {
      this.message.message = ''
      this.message.status = false
      this.message.color = ''
    }
  }
});