import { defineStore } from 'pinia';

export const useAuthStore = defineStore({
  id: 'taks',
  state: () => ({
    apiToken: ''
  }),
  actions: {
    setApiToken(value: string) {
      this.apiToken = value
    },
    clearApiToken() {
      this.apiToken = ''
    }
  }
});